import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import ContentHeader from "../../componentes/ContentHeader";
import Footer from "../../componentes/Footer";
import Navbar from "../../componentes/Navbar";
import SidebarContainer from "../../componentes/SidebarContainer";
import APIInvoke from "../../utils/APIInvoke";
import swal from "sweetalert";

const AdminEmpleados = () => {
    const [empleados, setEmpleados] = useState([]);

    const cargarEmpleados = async () => {
        const response = await APIInvoke.invokeGET("/api/empleados");

        setEmpleados(response);
    };

    useEffect(() => {
        cargarEmpleados();
    }, []);

    //Eliminar empleados
    const eliminarEmpleado = async (e, idEmpleado) => {
        e.preventDefault();
        const response = await APIInvoke.invokeDELETE(`/api/empleados/${idEmpleado}`);

        if (response.msg === 'empleado eliminado con exito') {
            const msg = "El empleado fue borrado correctamente.";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });
            cargarEmpleados();
        } else {
            const msg = "El empleado no fue borrado correctamente.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });
        }

    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">
                <ContentHeader
                    titulo={"Listado de Empleados"}
                    breadCrumb1={"Inicio"}
                    breadCrumb2={"Emplados"}
                    ruta1={"/home"}
                />

                <section className="content">
                    <div className="card">
                        <div className="card-header">
                            <h3 className="card-title"><Link to={"/crear-empleado"} className="btn btn-block btn-primary btn-sm">Crear Empleado</Link></h3>
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i className="fas fa-minus" />
                                </button>
                                <button type="button" className="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i className="fas fa-times" />
                                </button>
                            </div>
                        </div>
                        <div className="card-body">
                            <table className="table table-bordered">
                                <thead>
                                    <tr>
                                        <th style={{ width: "5%" }}>Id</th>
                                        <th style={{ width: "25%" }}>Nombre</th>
                                        <th style={{ width: "20%" }}>Cargo</th>
                                        <th style={{ width: "10%" }}>Teléfono</th>
                                        <th style={{ width: "15%" }}>Email</th>
                                        <th style={{ width: "15%" }}>Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {empleados.map((item) => (
                                        <tr key={item._id}>
                                            <td>{item._id}</td>
                                            <td>{item.nombre}</td>
                                            <td>{item.cargo}</td>
                                            <td>{item.telefono}</td>
                                            <td>{item.email}</td>
                                            <td>
                                                <Link to={`/editar-empleado/${item._id}@${item.nombre}@${item.cargo}@${item.telefono}@${item.email}`} className="btn btn-sm btn-primary">Editar</Link>&nbsp;&nbsp;

                                                <button onClick={(e) => eliminarEmpleado(e, item._id)} className="btn btn-sm btn-danger">Borrar</button>

                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>
    );
};

export default AdminEmpleados;
