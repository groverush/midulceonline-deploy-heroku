import React, { useState, useEffect } from 'react';
import ContentHeader from '../../componentes/ContentHeader';
import Footer from '../../componentes/Footer';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import { useNavigate, useParams } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke'
import swal from 'sweetalert';

const EditarCliente = () => {

    const navigate = useNavigate();

    const { idcliente } = useParams();
    let arreglo = idcliente.split('@');

    const nombreCliente = arreglo[1];
    const edadCliente = arreglo[2];
    const favoritosCliente = arreglo[3];
    const premiumCliente = arreglo[4];


    console.log(arreglo)


    const [cliente, setCliente] = useState({
        nombre: nombreCliente,
        edad: edadCliente,
        favoritos: favoritosCliente,
        premium: premiumCliente,

    });

    const { nombre, edad, favoritos, premium } = cliente;

    useEffect(() => {
        document.getElementById("nombre").focus();
    }, [])

    const onChange = (e) => {
        setCliente({
            ...cliente,
            [e.target.name]: e.target.value
        })
    }

    const editarCliente = async () => {
        let arreglo = idcliente.split('@');
        const idCliente = arreglo[0];


        const data = {
            nombre: cliente.nombre,
            edad: cliente.edad,
            favoritos: cliente.favoritos,
            premium: cliente.premium,

        }

        const response = await APIInvoke.invokePUT(`/api/clientes/${idCliente}`, data);
        const idProductoEditado = response._id

        if (idProductoEditado !== idCliente) {
            const msg = "El cliente no fue editado correctamente.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });

        } else {
            navigate("/admin-clientes");
            const msg = "El cliente fue editado correctamente.";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });
        }
    }

    const onSubmit = (e) => {
        e.preventDefault();
        editarCliente();
    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">

                <ContentHeader
                    titulo={"Edición de Clientes"}
                    breadCrumb1={"Listado de Clientes"}
                    breadCrumb2={"Edición"}
                    ruta1={"/admin-clientes"}
                />

                <section className="content">
                    <div className="card">
                        <div className="card-header">
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i className="fas fa-minus" />
                                </button>
                                <button type="button" className="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i className="fas fa-times" />
                                </button>
                            </div>
                        </div>
                        <div className="card-body">

                            <form onSubmit={onSubmit}>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Nombre</label>
                                        <input type="text"
                                            className="form-control"
                                            id="nombre"
                                            name="nombre"
                                            placeholder="Ingrese el nombre del cliente"
                                            value={nombre}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Edad</label>
                                        <input type="text"
                                            className="form-control"
                                            id="edad"
                                            name="edad"
                                            placeholder="Ingrese la edad del cliente"
                                            value={edad}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Dulces favoritos</label>
                                        <input type="text"
                                            className="form-control"
                                            id="favoritos"
                                            name="favoritos"
                                            placeholder="Ingrese los dulces favoritos del cliente"
                                            value={favoritos}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Premium</label>
                                        <input type="text"
                                            className="form-control"
                                            id="premium"
                                            name="premium"
                                            placeholder="Cliente premium (SI o NO)"
                                            value={premium}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>


                                <div className="card-footer">
                                    <button type="submit" className="btn btn-primary">Editar</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default EditarCliente;